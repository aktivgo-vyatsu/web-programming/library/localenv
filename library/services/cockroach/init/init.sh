#!/bin/sh

./cockroach sql --host="library_cockroach" --insecure --execute="CREATE USER IF NOT EXISTS cockroach; \
    CREATE DATABASE IF NOT EXISTS library; \
    GRANT ALL ON DATABASE library TO cockroach;"

migrate -path ./migrations -database "cockroachdb://cockroach@library_cockroach:26257/library?sslmode=disable" up