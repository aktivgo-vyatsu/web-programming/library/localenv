# Initialization
- `make init` - this clone all dependencies.

# Deployment
- `make start` - this deploy the application and roll migrations to the database.  
If errors occur, restart the deployment with the `make build` command.
In most cases this will solve the problems.


- frontend available at `80` port
- api available at `8080` port

# Stop and clean
- `make stop` - this down all dependencies and remove containers.
