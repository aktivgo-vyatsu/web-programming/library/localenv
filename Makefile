build:
	make stop && cd library && docker-compose up --build -d

start:
	cd library && docker-compose up -d

stop:
	cd library && docker-compose down || true

restart: stop start

init:
	(git clone https://gitlab.com/aktivgo-vyatsu/web-programming/library/front.git services/front || true) && \
	(git clone https://gitlab.com/aktivgo-vyatsu/web-programming/library/api.git services/api/api || true) && \
	(git clone https://gitlab.com/aktivgo-vyatsu/web-programming/library/migrations.git services/migrations || true)

pull:
	(cd services/api/api && git pull origin master || true) && \
	(cd services/front && git pull origin master || true) && \
	(cd services/migrations && git pull origin master || true)

migrate-up-all:
	migrate -path ./services/migrations -database "cockroachdb://cockroach@localhost:26257/library?sslmode=disable" up

migrate-down-all:
	migrate -path ./services/migrations -database "cockroachdb://cockroach@localhost:26257/library?sslmode=disable" down